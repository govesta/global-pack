const GOOGLE_MAPS_API = "AIzaSyCS4-oEErbzd_k6q-wi0bkKDrePWyOCN-8";
const SPLIT_IO_API_KEY = "654e849hnf2to92gql55vdsjss7m8b4kvkq";

const SOCIAL_LINKS = {
    INSTAGRAM: "https://www.instagram.com/govesta.co/",
    FACEBOOK: "https://www.facebook.com/govesta.co",
    TWITTER: "https://twitter.com/govesta_co",
    LINKEDIN: "https://www.linkedin.com/company/govesta"
}

const DOMAINS = {
    WEBSITE: 'https://govesta.co',
    DASHBOARD: 'https://dashboard.govesta.co'
}

const websiteurl = (path) => `${DOMAINS.WEBSITE}${path}`;
const dashboardurl = (path) => `${DOMAINS.DASHBOARD}${path}`;

const AGENCY_PHONE = '+49 151 47609576';
const AGENCY_EMAIL = 'agency@govesta.co';

const HUB_API = "1034e147-8b43-42d3-bf61-476ed74125f5";

const LANDING_HEADER_LINKS = [
    {
        text: "page.landing.agency.header.phone",
        link: "tel:+49 151 47609576",
        theme: "white-border",
        scrollTheme: "black-border",
        icon: "icon-phone"
    },
    {
        text: "page.landing.agency.header.email",
        link: "mailto:agency@govesta.co",
        theme: "brand",
        icon: "icon-email"
    }
]

const LANDING_HEADER_MENU_LINKS = [
    {
        text: "page.landing.agency.menu.whygovesta",
        route: {
           name: "landing",
           slug: "agency-why-govesta"
        }
    },
    {
        text: "page.landing.agency.menu.team",
        route: {
           name: "landing",
           slug: "agency-team"
        }
    },
    {
        text: "page.landing.agency.menu.contact",
        route: {
           name: "landing",
           slug: "agency-contact"
        } 
    }
]

const POPULAR_PROPERTIES_LIMIT = 15;
const PROPERTIES_PAGE_SIZE = 20;

const DEFAULT_AGENCY_PHONE = "+49 162 1543274";
const DEFAULT_AGENCY_EMAIL = "agency@govesta.co";

exports.LANDING_HEADER_MENU_LINKS = LANDING_HEADER_MENU_LINKS;
exports.LANDING_HEADER_LINKS = LANDING_HEADER_LINKS;
exports.SOCIAL_LINKS = SOCIAL_LINKS;
exports.GOOGLE_MAPS_API = GOOGLE_MAPS_API;
exports.DOMAINS = DOMAINS;
exports.websiteurl = websiteurl;
exports.dashboardurl = dashboardurl;
exports.AGENCY_PHONE = AGENCY_PHONE;
exports.AGENCY_EMAIL = AGENCY_EMAIL;
exports.HUB_API = HUB_API;
exports.POPULAR_PROPERTIES_LIMIT = POPULAR_PROPERTIES_LIMIT;
exports.PROPERTIES_PAGE_SIZE = PROPERTIES_PAGE_SIZE;
exports.SPLIT_IO_API_KEY = SPLIT_IO_API_KEY;
exports.DEFAULT_AGENCY_PHONE = DEFAULT_AGENCY_PHONE;
exports.DEFAULT_AGENCY_EMAIL = DEFAULT_AGENCY_EMAIL;
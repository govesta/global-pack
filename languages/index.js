
exports.getMessages = locale => {
    return require(`./locales/${locale}/index.js`)
}

exports.languages = ['en', 'de', 'fr']
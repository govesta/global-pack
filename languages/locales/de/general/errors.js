const Messages = {
"general.error.messages.required": "Dieses Feld ist ein Pflichtfeld",
"general.error.messages.common_server": "Ein Fehler ist aufgetreten",
"general.error.messages.already_taken": "Diese E-mail Adresse ist bereits vergeben.",
"general.error.messages.confirm": "Dieses Eingabe ist nicht identisch mit dem Feld{value}",
}

module.exports = {
    ...Messages
}


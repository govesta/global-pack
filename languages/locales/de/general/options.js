const Options = {
    "general.options.transaction_type.buy": "Kaufen",
    "general.options.transaction_type.rent": "Miete",
    "general.options.transaction_type.lease": "Pachtvertrag",
    "general.options.transaction_type.sell": "Verkaufen",
}

const PropertyCategory = {
    "general.options.property.features.category.luxury": "Luxury",    
    "general.options.property.features.category.simple": "Simple",    
    "general.options.property.features.category.upscale": "Upscale",    
    "general.options.property.features.category.normal": "Normal"
}

const PropertyParking = {
    "general.options.property.features.parking.carport": "Carport",    
    "general.options.property.features.parking.duplex": "Duplex",    
    "general.options.property.features.parking.openspace": "Open space",    
    "general.options.property.features.parking.garage": "Garage",    
    "general.options.property.features.parking.parkinggarage": "Parking garage",    
    "general.options.property.features.parking.undergroundparking": "Underground parking",    
    "general.options.property.features.parking.other": "Other"
}

const PropertyState = {
    "general.options.property.features.state.brandnew": "Brand new",    
    "general.options.property.features.state.demolitionobject": "Demolition object",    
    "general.options.property.features.state.buildingdue": "Building due",    
    "general.options.property.features.state.decored": "Decored",    
    "general.options.property.features.state.projected": "Projected",    
    "general.options.property.features.state.neu": "New",    
    "general.options.property.features.state.fullyrenovate": "Fully Renovate"
}

const PropertyFeatures = {
    "general.options.property.features.balcony": "Balkon",
    "general.options.property.features.bathtub": "Badewanne",
    "general.options.property.features.conceirge": "Conceirge",
    "general.options.property.features.design_home": "Design Immobilie",
    "general.options.property.features.disabled_access": "Behindertengerechter Zugang",
    "general.options.property.features.elevator": "Aufzug",
    "general.options.property.features.farm": "Bauernhöfe",
    "general.options.property.features.fireplace": "Kamin",
    "general.options.property.features.fitted_kitchen": "Einbauküche",
    "general.options.property.features.furnished": "Möbliert",
    "general.options.property.features.garage": "Werkstatt",
    "general.options.property.features.garden": "Garten",
    "general.options.property.features.holiday_home": "Ferienhaus",
    "general.options.property.features.land": "Land",
    "general.options.property.features.new_home": "Neues Zuhause",
    "general.options.property.features.parking_space": "Parkplätze",
    "general.options.property.features.period_home": "Zeitraum home",
    "general.options.property.features.pool": "Pool",
    "general.options.property.features.retirement_home": "Seniorenresidenz",
    "general.options.property.features.roof_terrace": "Dachterrasse",
    "general.options.property.features.serviced": "Betreut",
    "general.options.property.features.suitable_for_families": "geeignet für Familien",
    "general.options.property.features.suitable_for_pets": "geeignet für Haustiere",
    "general.options.property.features.suitable_for_sharers": "geeignet für Sharers",
    "general.options.property.features.suitable_for_students": "geeignet für Studenten",
    "general.options.property.features.terrace ": "Terrasse",
    "general.options.property.features.unfurnished": "Unmöbliert",    
}


module.exports = {
    ...Options,
    ...PropertyFeatures,
    ...PropertyCategory,
    ...PropertyParking,
    ...PropertyState
}

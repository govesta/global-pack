const Date = {
    "general.date.day": "{value, plural, one {# Tag} other {# Tage}}"
}
 
module.exports = {
    ...Date
}

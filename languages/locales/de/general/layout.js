﻿const Header = {
    "layout.header.links.agency": "Immobilien bewerben",
    "layout.header.links.magazine": "Magazin",
    "layout.header.links.dashboard": "Anmelden",
    "layout.header.menu.apartments": "Wohnungen",
    "layout.header.menu.houses": "Häuser",
    "layout.header.menu.holyday_homes": "Ferienhäuser",
    "layout.header.menu.luxury_properties": "Luxusimmobilien",
}

const Footer = {
    "layout.footer.copyright": "© getmynewhome UG",
    "layout.footer.links.imprint": "Impressum",
    "layout.footer.links.terms": "Geschäftsbedingungen",
    "layout.footer.links.privacy_policy": "Datenschutzerklärung",
}

const Seo = {
    "layout.seo.general.title": "Startseite",
    "layout.seo.general.description": "Finden Sie mit Govesta Ihr perfektes neues Zuhause auf der ganzen Welt. ✓ Häuser, ✓ Wohnungen ✓ Gewerbeimmobilien zum Kaufen und Mieten.",
    "layout.seo.search.title": "Suche",
    "layout.seo.search.title.with_location": "{location}",
    "layout.seo.page.title": "{page}",
    "layout.seo.page.search.title": "Immobilien zum Verkauf in {location} - {number_of_properties} Immobilien zum Verkauf",
    "layout.seo.page.search.meta": "Finden Sie Immobilien zum Verkauf in {location} mit Govesta.co. Wählen Sie aus unseren {number_of_properties} Immobilien. Nutzen Sie unsere Filter, um Ihr neues Zuhause zu finden.",
}

const Forward = {
    "layout.forward.text": "Sie haben eine tolle Immobilie auf Govesta gefunden..<br/><br/> Wir leiten Sie jetzt zu { value } weiter."
}


module.exports = {
    ...Seo,
    ...Header,
    ...Footer,
    ...Forward
}


﻿const Cookie = {
    "component.eucookie.text": "Govesta verwendet Browser-Cookies, um Ihnen das bestmögliche Erlebnis zu bieten. Damit Govesta funktioniert, protokollieren wir Benutzerdaten und geben sie an Datenverarbeiter weiter. Um Govesta nutzen zu können, müssen Sie unserer <a>Datenschutzerklärung zustimmen.</a>.",
    "component.eucookie.button": "Ich stimme zu."
}
const Address = {
    "component.address.placeholder": "Such Nach Ort, Stadt oder Stadtteil",
    "component.address.placeholder_question": "Where do you want to buy?"
}
const PropertyCard = {
    "component.property_card.view": "Details"
}

module.exports = {
    ...Cookie,
    ...Address,
    ...PropertyCard
}


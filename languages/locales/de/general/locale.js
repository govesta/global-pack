const Languages = {
    "general.language.en": "Englisch",
    "general.language.de": "Deutsch",
    "general.language.es": "Spanisch",
    "general.language.fr": "Französisch",
    "general.language.it": "Italienisch "
}

module.exports = {
    ...Languages
}
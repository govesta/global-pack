const NewProperty = {
    "page.property.new.section.translation.new.title": "Übersetzung",
    "page.property.new.section.translation.title": "Übersetzung",
    "page.property.new.section.translation.edit.title": "{language} Übersetzung",
    "page.property.new.section.translation.save": "Übersetzung speichern",
    "page.property.new.section.translation.add": "+ Neue Sprache hinzufügen",
    "page.property.new.section.details.title": "Details",
    "page.property.new.section.features.title": "Features",
    "page.property.new.section.location.title": "Lage",
    "page.property.new.section.price.title": "Preis",
    "page.property.new.section.images.title": "Bilder",
    "page.property.new.section.images.add": "Bilder hinzufügen",
    "page.property.new.section.floor.plan.title": "Grundriss",
    "page.property.new.section.floor.plan.add": "Grundriss hinzufügen",
    "page.property.new.actions.cancel": "Abbrechen",
    "page.property.new.actions.save": "Änderungen speichern",
    "page.property.new.actions.back": "Zurück zu Meine Objekte",
    "page.property.new.actions.saved": "Gespeichert!",
    "page.property.new.actions.start": "Listing starten",
    "page.property.new.alert.start.title": "Möchtest du dein Govesta Listing starten?",
    "page.property.new.alert.start.button.no": "Nein, noch nicht.",
    "page.property.new.alert.start.button.yes": "Ja, los geht's!",
    "page.property.new.alert.pause.title": "Are you sure you want to Pause your listing?",
    "page.property.new.alert.pause.button.no": "No keep it running",
    "page.property.new.alert.pause.button.yes": "Yes, pause it!",
    "page.property.new.alert.back.title": "Erst spiechern?",
    "page.property.new.alert.back.button.no": "Nein",
    "page.property.new.alert.back.button.yes": "Ja, erst speichern",
    "page.property.new.alert.delete.title": "Bist du sicher, dass du diesen Eintrag löschen möchtest?",
    "page.property.new.alert.delete.button.no": "Nein, behalte es.",
    "page.property.new.alert.delete.button.yes": "Ja, löscht es.",
    "page.property.new.inputs.language.placeholder": "Sprache auswählen",
    "page.property.new.inputs.title.placeholder": "Dein Objektstitel",
    "page.property.new.inputs.link.placeholder": "Link zu deiner Objekt-Details Seite",
    "page.property.new.inputs.description.placeholder": "Kurze Beschreibung des Objekts",
    "page.property.new.inputs.transaction_type.placeholder": "- Transaktionsart auswählen - ",
    "page.property.new.inputs.type.placeholder": "- Immobilientyp auswählen - ",
    "page.property.new.inputs.sub_type.placeholder": "- Immobilien-Subtyp auswählen - ",
    "page.property.new.inputs.rooms.placeholder": "Anzahl der Räume",
    "page.property.new.inputs.sqm.placeholder": "QM",
    "page.property.new.inputs.bedrooms.placeholder": "Schlafzimmer",
    "page.property.new.inputs.bathrooms.placeholder": "Badezimmer",
    "page.property.new.inputs.street.placeholder": "Straße",
    "page.property.new.inputs.street_number.placeholder": "Hausnummer",
    "page.property.new.inputs.postal_code.placeholder": "Postleitzahl",
    "page.property.new.inputs.city.placeholder": "Stadt",
    "page.property.new.inputs.state.placeholder": "Bundesland",
    "page.property.new.inputs.price.placeholder": "Preis",
    "page.property.new.inputs.year_built.label": "Year Built",
    "page.property.new.inputs.year_built.placeholder": "Year Built",
    "page.property.new.inputs.property_category.label": "Property Category",
    "page.property.new.inputs.property_category.placeholder": "Property Category",
    "page.property.new.inputs.type_of_state.label": "Type of State",
    "page.property.new.inputs.type_of_state.placeholder": "Type of State",
    "page.property.new.inputs.parking_space.label": "No. of Parking spaces",
    "page.property.new.inputs.parking_space.placeholder": "No. of Parking spaces",
    "page.property.new.inputs.parking_space_type.label": "Parking space type",
    "page.property.new.inputs.parking_space_type.placeholder": "Parking space type",
    "page.property.new.inputs.balconies.label": "No. of Balconies",
    "page.property.new.inputs.balconies.placeholder": "No. of Balconies ",
    "page.property.new.inputs.terraces.label": "No. of Terraces",
    "page.property.new.inputs.terraces.placeholder": "No. of Terraces",
    "page.property.new.inputs.other_features.title": "Other Features",
    "page.property.new.header.back": "Abbrechen, zurück zur Übersicht"
}

const Properties = {
    "page.property.list.title": "Deine Objekte",
    "page.property.list.empty_text": "Fange mit dem Hinzufügen von Objekten an.",
    "page.property.list.column.edit_details": "Details bearbeiten",
    "page.property.list.column.days_online": "Tage Online",
    "page.property.list.column.ad_status": "Ad Status",
    "page.property.list.button.add_link": "+ Link hinzufügen",
    "page.property.list.show_more.button": "Alle Objekte anzeigen",
    "page.property.list.show_less.button": "Weniger Objekte anzeigen",
    "page.property.list.add": "+ Objekt hinzufügen ",
    "page.property.list.synced.title": "Synchronisierte Objekte ({total})",
    "page.property.list.active.title": "Aktive Objekte ({total})",
    "page.property.list.inactive.title": "Inaktive Objekte ({total})",
    "page.property.list.deleted.title": "Gelöschte Objekte ({total})",
    "page.property.list.last_edit": "Letzte Bearbeitung",
    "page.property.list.pagination.next" : "Next",
    "page.property.list.pagination.back" : "Back"
}

const Search = {
    "page.property.search.filter.clear": "Filter löschen",
    "page.property.search.sorting.standard": "Standard",
    "page.property.search.sorting.price": "Nach Preis sortieren",
    "page.property.search.sorting.sqmprice": "Sortieren nach Preis pro m2",
    "page.property.search.sorting.date": "Sortieren nach Datum hinzugefügt",
    "page.property.search.total.info": "{value} Eigenschaften gefunden",
    "page.property.search.pagination.info.top": "{from} – {to} von {total} Immobilien",
    "page.property.search.pagination.info.bottom": "Finden Sie ihr neues zuhause mit Govesta.",
    "page.property.search.filter.mobile.title": "Alle Filter",
    "page.property.search.filter.mobile.see_all": "Alle anzeigen",
    "page.property.search.location.placeholder": "e.g. Neukölln, Mitte, ...",
    "page.property.search.info.top":"Choose your property type to get more relevant results for yourself.",
    "page.property.search.info.text": "In the last 100 days already more than {total} homes got sold in {location}",
    "page.property.search.no_results.title": "Keine Ergebnisse",
    "page.property.search.no_results.description": "Versuchen Sie, Ihre Suche anzupassen, indem Sie Ihre Daten ändern, Filter entfernen.",
    "page.property.search.no_results.clear.button": "Alle Filter entfernen",
    "page.property.search.rooms" : "Zimmer",
    "page.property.search.sqm" : "Größe",
    "page.property.search.bedrooms" : "Schlafzimmer",
    "page.property.search.bathrooms" : "Badezimmer",
    "page.property.search.price" : "Preis",
    "page.property.search.default_property_type" : "Objekte",
    "page.property.search.default_location" : "Europa",
    "page.property.search.header_main" : "Immobilien in",
}

const PropertyDetail = {
    "page.property.detail.carousel.view.all": "View All Photos",
    "page.property.detail.carousel.share": "Teilen",
    "page.property.detail.content.read.more":"Mehr lesen",
    "page.property.detail.content.location.title":"Lage",
    "page.property.detail.content.feature.more":"Alle Austattungen zeigen",
    "page.property.detail.box.button":"Kontakt zur Agentur",
    "page.property.detail.box.info.title":"Dies ist eine sehr schöne Wohnung!.",
    "page.property.detail.box.info.description":"Es wurde in der letzten Woche {value} mal angesehen.",
    "page.property.detail.properties.title": "Mehr Wohnungen in {value}",
    "page.property.detail.properties.button": "Alle zeigen ({value}+)",
    "page.property.list.autocomplete.placeholder":"Search for Property"
}

const AgencyRequest = {
    "page.property.agency_request.terms_of_use" : "Bitte beachten Sie, dass Govesta die obigen Angaben nur an den Partner sendet, der diese Immobilie verkauft.",
    "page.property.agency_request.call": "Makler anrufen",
    "page.property.agency_request.message_placeholder": "Ich habe Interesse an folgender Immobilie ",
    "page.property.agency_request.success": "Ihre Nachrcht wurde an den Makler weitergeleiter, er wird sich in Kürze melden.",
    "page.property.agency_request.title": "Makler kontaktieren"
}

module.exports = {
    ...PropertyDetail,
    ...Search,
    ...NewProperty,
    ...Properties,
    ...AgencyRequest
}

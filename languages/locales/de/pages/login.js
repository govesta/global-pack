﻿const Login = {
"page.login.title": "Melden Sie sich bei Ihrem Konto an",
"page.login.sign_up_text": "Sie haben noch kein Konto?",
"page.login.error": "E-Mail oder Passwort ist nicht korrekt",
"page.login.social.facebook.button": "Mit Facebook einloggen",
"page.login.social.google.button": "Mit Google einloggen",
"page.login.forgot_password.link": "Passwort vergessen?",
}

const Register = {
"page.register.title": "Erste Schritte mit einem kostenlosen Konto",
"page.register.log_in_text": "Hast du ein Konto?",
"page.register.social.facebook.button": "Registrieren Sie sich mit Facebook",
"page.register.social.google.button": "Registrieren Sie sich mit Google",
}

const ProfileEdit = {
"page.profile.edit.general_information.title": "Allgemeine Informationen",
"page.profile.edit.address_information.title": "Adresse Informationen",
"page.profile.edit.password_information.title": "Passwort Informationen",
"page.profile.edit.privacy.title": "Datenschutz",
"page.profile.edit.error.password_match": "Passwörter stimmen nicht überein",
}

const ForgotPassword = {
"page.forgot_password.title": "Passwort zurücksetzen",
"page.forgot_password.button": "Link zum zurücksetzen senden",
"page.forgot_password.back_to_login": "Zurück zum Login",
"page.forgot_password.success_message": "Link zum zurücksetzen senden erfolgreich versand",
"page.forgot_password.user_not_found": "E-Mail Adresse nicht gefunden",
}

const ForgotPasswordVerify = {
    "page.forgot_password_verify.title": "Neues Passwort",
    "page.forgot_password_verify.button": "Speichern",
    "page.forgot_password_verify.success_message": "Passwort erfolgreich zurück gesetzt",
    "page.forgot_password_verify.confirm_error": "Diese Eingabe ist nicht mit dem Passwort identisch.",
    }

module.exports = {
    ...Login,
    ...Register,
    ...ProfileEdit,
    ...ForgotPassword,
    ...ForgotPasswordVerify
}



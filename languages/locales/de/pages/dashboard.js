const Dashboard = {
    "page.dashboard.hi.title": "Hallo, {value}",
    "page.dashboard.hi.description": "Hier finden Sie eine Übersicht über alle wichtigen Dinge, die mit deinem Govesta-Konto zu tun haben.",
    "page.dashboard.listings.title": "Objekte",
    "page.dashboard.listings.active": "Aktive Objekte",
    "page.dashboard.listings.pause": "Pausierte Objekte",
    "page.dashboard.listings.total": "Gesamt Objekte",
    "page.dashboard.analytics.title": "Analytik",
    "page.dashboard.analytics.link.1": "Aktueller Monat",
    "page.dashboard.analytics.link.2": "Letzte 3 Monate",
    "page.dashboard.analytics.link.3": "Alle Daten",
    "page.dashboard.account.title": "Konto",
    "page.dashboard.coming_soon": "(In Kürze verfügbar)",
}

module.exports = {
    ...Dashboard
}

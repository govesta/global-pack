const _404 = {
    "page.404.title": "Oops!",
    "page.404.description": "Wir können die gesuchte Seite nicht finden..",
    "page.404.error_code": "Fehlercode: {code}",
    "page.404.links_title": "Hier sind statt dessen einige hilfreiche Links:",
    "page.404.link.home": "Startseite",
    "page.404.link.search": "Suche "
}

module.exports = {
    ..._404
}


const AgencyHeader = {
    "page.landing.agency.menu.whygovesta": "Warum Govesta?",
    "page.landing.agency.menu.team": "Unser Team",
    "page.landing.agency.menu.help": "Hilfe",
    "page.landing.agency.menu.contact": "Kontakt",
    "page.landing.agency.header.text": "Wenn Sie Fragen haben, wenden Sie sich an uns.",
    "page.landing.agency.header.phone": "Anruf",
    "page.landing.agency.header.email": "E-mail",
}

const Agency = {
    "page.landing.agency.title": "Holen Sie sich Käufer auf Ihre Website.",
    "page.landing.agency.description": "Die Immobiliensuche von Govesta verbindet Käufer und Verkäufer auf der ganzen Welt",
    "page.landing.agency.button": "Erfahren Sie mehr",
        
    "page.landing.agency.challenge.title": "Gehe mit der Zeit",
    "page.landing.agency.challenge.description": "Wir haben die Lösung auf altbekannte Probleme:<br><br>✖  Überteuerte Abonnements<br>✖  Langfristige Verträge<br>✖  Kein Markenengagement<br>✖  Schlechte Benutzererfahrung<br>✖  Keine Internationalisierung",
    
    "page.landing.agency.different.title": "Was unterscheidet Govesta von anderen Immobilienportalen?",
    "page.landing.agency.different.description": "",
    "page.landing.agency.different.button": "Nehmen Sie Kontakt auf",
   
    "page.landing.agency.different.1.titel": "Ihre Kunde",
    "page.landing.agency.different.1.description": "Wenn sich jemand für Ihre Immobilie interessiert, senden wir den Kunden direkt auf Ihre Website. Immerhin sind es Ihre Kunden, nicht unsere!",
    "page.landing.agency.different.2.titel": "International",
    "page.landing.agency.different.2.description": "Überall kaufen, überall verkaufen. Unsere Website ist in mehreren Sprachen verfügbar und wir fügen jeden Monat weitere hinzu.",
    "page.landing.agency.different.3.titel": "Fair & Flexibel",
    "page.landing.agency.different.3.description": "Sie entscheiden, wann Sie werben wollen. Es gibt keine langfristige Vertragsbindung, keine Abonnementgebühren, und so wird es immer sein.",

    "page.landing.agency.grow.title": "Hochwertige Kundenansprache",
    "page.landing.agency.grow.description": "Zeigen Sie Ihren Kunden, worum es Ihnen geht.",
    "page.landing.agency.grow.quote": "„Govesta bringt potenzielle Käufer auf meine Website, dort wo ich sie haben möchte!<br>So kann ich meine Inhalte richtig präsentieren, meine Besucher erneut ansprechen und bei SEM Geld sparen, weil Govesta das für mich erledigt.“",
    "page.landing.agency.grow.quotedesc": "Thirzie Hull –  Immobilienmaklerin, London",
    
    "page.landing.agency.setup.title": "Der Einstieg ist einfach",
    "page.landing.agency.setup.description": "Wir sind mit Openimmo synchronisiert und werden auf Plattformen wie OnOffice, FlowFact und Propstack verwendet. Wir helfen Ihnen, die beste Anbindung zu finden.",
    
    "page.landing.agency.trial.title": "Nehmen Sie Kontakt auf",
    "page.landing.agency.trial.description": "Frag uns alles! Wir werden uns umgehend mit Ihnen in Verbindung setzen.",

    "page.landing.agency.team.title": "Unser Team",
    "page.landing.agency.team.description": "⠀",

    "page.landing.agency.faq.title": "Häufig gestellte Fragen",

    "page.landing.agency.faq.question1": "Wie viel kostet Werbung bei Govesta?",
    "page.landing.agency.faq.answer1": "Das Hochladen Ihrer Immobilien auf Govesta ist völlig kostenlos. Sie zahlen nur, wenn jemand auf einen Ihrer Einträge klickt und auf Ihre Website gelangt.",

    "page.landing.agency.faq.question2": "Kann ich auf anderen Plattformen werben?",
    "page.landing.agency.faq.answer2": "Ja, Sie können auf so vielen anderen Plattformen werben, wie Sie möchten.",

    "page.landing.agency.faq.question3": "Ist die Anzahl der Immobilien, die ich gleichzeitig hochladen kann, begrenzt?",
    "page.landing.agency.faq.answer3": "Uploads sind unbegrenzt. Sie können beliebig viele Objekte hochladen und die Anzeige jederzeit von der Seite nehmen.",

    "page.landing.agency.faq.question4": "Kann ich ein maximales Budget für meine Angebote festlegen?",
    "page.landing.agency.faq.answer4": "Sie können jede Anzeige jederzeit pausieren oder löschen, jedoch kein maximales Budget festlegen.",

    "page.landing.agency.faq.question5": "Brauche ich eine eigene Website, um bei Govesta zu werben?",
    "page.landing.agency.faq.answer5": "Bei Govesta dreht sich alles um die Verbindung zwischen Ihnen und ihren Kunden. Sie benötigen eine Website, um Ihr Portfolio zu präsentieren.",
    "page.landing.agency.contact.submit": "Nehmen Sie Kontakt auf",
    "page.landing.agency.contact.success_message": "Zugesandt!",
    
    "page.landing.agency.contact.title": "Nehmen Sie Kontakt auf",
    "page.landing.agency.contact.decription": " ",



    "page.landing.agency.help.title": "Lassen Sie uns Ihnen gelfen das Beste aus Govesta heraus zu holen",
    "page.landing.agency.help.button": "Mehr",



    "page.landing.agency.why.1.title": "1. Laden Sie alle Ihre Angebote hoch, kostenlos.",
    "page.landing.agency.why.1.text": "Egal, ob Sie 1 Immobilie zum Verkauf haben oder 1000, Sie können so viele Angebote hochladen, wie Sie möchten, kostenlos. Wir berechnen keine Start-up- oder Abonnementgebühren. Wir sind davon überzeugt, dass die Performance jedes Listing entscheidend ist.",
    "page.landing.agency.why.2.title": "2. Werben Sie zu Ihren Bedingungen",
    "page.landing.agency.why.2.text": "Wir verpflichten Sie nicht für ein oder zwei Jahre zu einem Werbevertrag. Deshalb kann Ihre Partnerschaft mit uns jederzeit und aus beliebigem Grund unterbrochen werden. Sie können alle Ihre Angebote individuell im Govesta Dashboard verwalten, so dass Sie immer die Kontrolle über Ihre Kosten behalten.",
    "page.landing.agency.why.3.title": "3. Holen Sie sich den Kunden direkt auf Ihre Website.",
    "page.landing.agency.why.3.text": "Wie viel haben Sie in Ihre brandneue Website investiert? Wenn du wie wir bist, dann ist es wahrscheinlich eine Menge, und Ihre Kunden sollten die Möglichkeit haben, es zu sehen! Wenn jemand ein Interesse an Ihrer Immobilie zeigt, sendet Govesta diese direkt auf Ihre Website. So können Sie beispielsweise Ihre Website entsprechend dem Nutzerverhalten optimieren, Ihre Besucher neu ausrichten oder ein ähnliches Publikum auf anderen Werbekanälen schaffen. Sie haben die Wahl!",
    "page.landing.agency.why.4.title": "4. Internationale Käufer",
    "page.landing.agency.why.4.text": "Kaufen Sie überall, verkaufen Sie überall! Das ist unsere Vision. Unser Kundenstamm ist international und kommt hauptsächlich aus europäischen Ländern. Mit Govesta sind Sie auf den besten Immobilienmärkten der Welt präsent und haben einfachen Zugang zu ausländischen Käufern. Unser Team verfügt über mehr als 10 Jahre Berufserfahrung in der digitalen Werbung. Wir wissen, wie man die richtigen Leute für Ihre Immobilie findet.",
    "page.landing.agency.why.5.title": "5. Verbunden mit der besten Property Management Software",
    "page.landing.agency.why.5.text": "Wir sind mit den besten Anbietern von Property Management Software verbunden, darunter OnOffice, Flowfact und Propstack. Und wenn Sie mehr als 100 Objekte zu bewerben haben, entwickeln wir eine vollständig mit Ihrer Datenbank verbundene API ohne zusätzliche Kosten.",
    "page.landing.agency.why.6.title": "6. Verfolgen Sie die Performance Ihrer Anzeige in Echtzeit.",
    "page.landing.agency.why.6.text": "Wir sind ein Online-Marketing-Technologieunternehmen. Wir verstehen, dass ein datengetriebener Ansatz eine erfolgreiche Werbekampagne ausmacht. Deshalb geben wir jedem Werbetreibenden freien Zugang zu unserem Performance Dashboard, wo Sie die Immobilienperformance in Echtzeit verfolgen können. Verstehen Sie, woher Ihre Besucher kommen, aber auch, woher Ihre Kosten kommen, und optimieren Sie.",
    "page.landing.agency.why.7.title": "7. Ihre Angebote auf dem besten Produkt",
    "page.landing.agency.why.7.text": "Die Mission von Govesta ist es, die beste Immobiliensuchplattform der Welt zu sein. Um dies zu erreichen, konzentrieren wir uns darauf, die bestmögliche Benutzererfahrung zu schaffen, damit Menschen mit wenigen Klicks ihr Traumhaus finden. Ebenso wichtig ist es, eine großartige Marke zu schaffen. Wir entwickeln unsere eigene Marke durch hochwertige, einzigartige Inhalte, die wir im Govesta Journal veröffentlichen. Werbung auf Govesta verstärkt Ihre Marke, anstatt sie zu schwächen.",
    "page.landing.agency.why.8.title": "8. Fachkundige Beratung zur Verbesserung Ihrer Website",
    "page.landing.agency.why.8.text": "Sobald Sie mit Govesta zusammenarbeiten, analysiert unser Expertenteam gerne Ihre Website und gibt Ratschläge, wie Sie sich verbessern können. Manchmal kann schon eine kleine Änderung einen großen Unterschied machen. Genießen Sie kostenlose und kompetente Beratung bei der Zusammenarbeit mit Govesta.",
    "page.landing.agency.why.9.title": "9. Schließen Sie sich der nächsten großen Sache in der Immobilienbranche an.",
    "page.landing.agency.why.9.text": "Wir sind jung, frisch und bereit, den Immobilienmarkt zu revolutionieren. Nach 10 Jahren in der Online-Industrie stellt das Govesta-Team seine Köpfe zusammen, um eine leistungsstarke Plattform für Käufer und Verkäufer aufzubauen. In den nächsten 2 Jahren planen wir ein sehr starkes europäisches Wachstum und wollen, dass unsere ersten Partner die nächsten bleiben.",
    "page.landing.agency.why.10.title": "10. Govesta kostenlos testen",
    "page.landing.agency.why.10.text": "Melden Sie sich bei Govesta an und erhalten Sie Ihre ersten 100 Kunden kostenlos. Es gibt keine langfristige Bindung und damit auch kein Risiko. Wir können es kaum erwarten, dich an Bord zu haben!",

}

module.exports = {
    ...Agency,
    ...AgencyHeader
}

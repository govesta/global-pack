const Languages = {
    "general.language.en": "English",
    "general.language.de": "German",
    "general.language.es": "Spanish",
    "general.language.fr": "French",
    "general.language.it": "Italian"
}

module.exports = {
    ...Languages
}
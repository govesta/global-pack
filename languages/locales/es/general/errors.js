const Messages = {
"general.error.messages.required": "Este campo es requerido",
"general.error.messages.common_server": "Ocurrió un error",
"general.error.messages.already_taken": "Esto ya tomada",
}

module.exports = {
    ...Messages
}

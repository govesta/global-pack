const Enum = {
"general.enum.status.disabled": "Discapacitado",
"general.enum.status.enabled": "Activado",
"general.enum.status.pending": "Pendiente",
"general.enum.status.draft": "Borrador",
"general.enum.status.published": "Publicado",
"general.enum.status.deleted": "suprimido",
"general.enum.status.confirmed": "Confirmado",
}

const Terms = {
"general.term.publish": "Publicar",
"general.term.unpublish": "Despublicar",
"general.term.edit": "Editar",
"general.term.date": "Fecha",
"general.term.title": "Título",
"general.term.status": "Estado",
"general.term.email_address": "E-mail",
"general.term.password": "Contraseña",
"general.term.log_in": "Iniciar sesión",
"general.term.sign_up": "Regístrate",
"general.term.first_name": "Nombre de pila",
"general.term.last_name": "Apellido",
"general.term.company_name": "nombre de empresa",
"general.term.agency": "Agencia",
"general.term.client": "Cliente",
"general.term.website": "Sitio web",
"general.term.save": "Salvar",
"general.term.about": "Acerca de",
"general.term.confirm_password": "Confirmar contraseña",
"general.term.cover_photo": "Foto de cubierta",
"general.term.logo": "Logo",
"general.term.back": "Espalda",
"general.term.type": "Tipo",
"general.term.sub_type": "Tipo sub",
"general.term.description": "Descripción",
"general.term.country": "País",
"general.term.city": "Ciudad",
"general.term.state": "Estado",
"general.term.street": "Calle",
"general.term.street_number": "Número de calle",
"general.term.price": "Precio",
"general.term.postal_code": "Código postal",
"general.term.currency": "Moneda",
"general.term.transaction_type": "tipo de transacción",
"general.term.rooms": "Total habitaciones",
"general.term.bedrooms": "Bed Rooms",
"general.term.bathrooms": "Las habitaciones de baño",
"general.term.next_step": "Próximo paso",
"general.term.show_password": "Mostrar contraseña",
"general.term.hide_password": "Contraseña oculta",
}

const Links = {
"general.link.add_property": "Agregar propiedad",
"general.link.properties": "Mis propiedades",
}

module.exports = {
    ...Links,
    ...Enum,
    ...Terms
}

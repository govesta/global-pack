const Options = {
"general.options.transaction_type.buy": "Comprar",
"general.options.transaction_type.rent": "Alquilar",
"general.options.transaction_type.lease": "Arrendamiento",
"general.options.transaction_type.sell": "Vender",
}

module.exports = {
    ...Options
}

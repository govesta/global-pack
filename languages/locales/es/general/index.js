const options = require('./options');
const errors = require('./errors');
const general = require('./general');
const locale = require('./locale');

module.exports = {
    ...locale,
    ...general,
    ...options,
    ...errors
}
const Login = {
    "page.login.title": "Log-in to your Account",
    "page.login.sign_up_text": "Don’t have an account?",
    "page.login.error": "Email or Password is not correct"
}

const Register = {
    "page.register.title": "Let's Register",
    "page.register.log_in_text": "Do you have an account?",
}

const ProfileEdit = {
    "page.profile.edit.general_information.title": "General Information",
    "page.profile.edit.address_information.title": "Address Information",
    "page.profile.edit.password_information.title": "Password Information",
    "page.profile.edit.privacy.title": "Privacy",
    "page.profile.edit.error.password_match": "Confirm password doesn't match "
}

module.exports = {
    ...Login,
    ...Register,
    ...ProfileEdit
}
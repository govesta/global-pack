const property = require('./property');
const login = require('./login');
const search = require('./search');

module.exports = {
    ...search,
    ...property,
    ...login
}
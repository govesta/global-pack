const PropertySearch = {
    "page.property.search.location.placeholder": "Select country, state, city, district"
}

module.exports = {
    ...PropertySearch
}

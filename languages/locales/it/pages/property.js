const NewProperty = {
    "page.property.new.steps_bar.basic_information": "Basic Information",
    "page.property.new.steps_bar.description": "Description",
    "page.property.new.steps_bar.location": "Location",
    "page.property.new.steps_bar.gallery": "Gallery",
    "page.property.new.steps_bar.floor_plan": "Floor Plan",
    "page.property.new.steps_bar.price": "Price",
    "page.property.new.steps.action.next": "Next Step",
    "page.property.new.steps.action.next_and_publish": "Save & Publish",
    "page.property.new.steps.inputs.transaction_type.placeholder": "Buy / Sell ?",
    "page.property.new.steps.inputs.type.placeholder": "House / Flat ?",
    "page.property.new.steps.inputs.sub_type.placeholder": "Sub Type ?",
    "page.property.new.steps.inputs.title.placeholder": "Title",
    "page.property.new.steps.inputs.description.placeholder": "Description",
    "page.property.new.steps.inputs.rooms.placeholder": "Total Rooms",
    "page.property.new.steps.inputs.bedrooms.placeholder": "Bed Rooms",
    "page.property.new.steps.inputs.bathrooms.placeholder": "Bath Rooms",
    "page.property.new.steps.inputs.street.placeholder": "Street",
    "page.property.new.steps.inputs.street_number.placeholder": "Street Number",
    "page.property.new.steps.inputs.postal_code.placeholder": "Post Code",
    "page.property.new.steps.inputs.city.placeholder": "City",
    "page.property.new.steps.inputs.state.placeholder": "State",
    "page.property.new.steps.inputs.price.placeholder": "Price",
    "page.property.new.steps.step1.title": "Hello, tell us some details about the lising.",
    "page.property.new.steps.step1.sub_title": "Step 1",
    "page.property.new.steps.step1.information_title": "What are the main information?",
    "page.property.new.steps.step2.title": "Give us some words about your listing.",
    "page.property.new.steps.step2.sub_title": "Step 2",
    "page.property.new.steps.step2.information_title": "Just a few words about the listing.",
    "page.property.new.steps.step3.title": "Give us some words about your listing.",
    "page.property.new.steps.step3.sub_title": "Step 3",
    "page.property.new.steps.step3.information_title": "Just a few words about the listing.",
    "page.property.new.steps.step4.title": "Give us some words about your listing.",
    "page.property.new.steps.step4.sub_title": "Step 4",
    "page.property.new.steps.step4.information_title": "Just a few words about the listing.",
    "page.property.new.steps.step4.add_files": "Add Files",
    "page.property.new.steps.step6.title": "Give us some words about your listing.",
    "page.property.new.steps.step6.sub_title": "Step 6",
    "page.property.new.steps.step6.information_title": "Just a few words about the listing."

}

module.exports = {
    ...NewProperty
}
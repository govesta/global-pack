const Enum = {
"general.enum.status.disabled": "Disabilitato",
"general.enum.status.enabled": "Abilitato",
"general.enum.status.pending": "in attesa di",
"general.enum.status.draft": "Bozza",
"general.enum.status.published": "Pubblicato",
"general.enum.status.deleted": "eliminata",
"general.enum.status.confirmed": "Confermato",
}

const Terms = {
"general.term.publish": "Pubblicare",
"general.term.unpublish": "Non pubblicato",
"general.term.edit": "modificare",
"general.term.date": "Data",
"general.term.title": "Titolo",
"general.term.status": "Stato",
"general.term.email_address": "E-mail",
"general.term.password": "Parola d'ordine",
"general.term.log_in": "Accesso",
"general.term.sign_up": "Iscriviti",
"general.term.first_name": "Nome di battesimo",
"general.term.last_name": "Cognome",
"general.term.company_name": "Nome della ditta",
"general.term.agency": "Agenzia",
"general.term.client": "Cliente",
"general.term.website": "Sito web",
"general.term.save": "Salvare",
"general.term.about": "Di",
"general.term.confirm_password": "conferma password",
"general.term.cover_photo": "Foto di copertina",
"general.term.logo": "Logo",
"general.term.back": "Indietro",
"general.term.type": "genere",
"general.term.sub_type": "Sottotipo",
"general.term.description": "Descrizione",
"general.term.country": "Nazione",
"general.term.city": "Città",
"general.term.state": "Stato",
"general.term.street": "strada",
"general.term.street_number": "Numero civico",
"general.term.price": "Prezzo",
"general.term.postal_code": "codice postale",
"general.term.currency": "Moneta",
"general.term.transaction_type": "Tipo di transazione",
"general.term.rooms": "Totale camere",
"general.term.bedrooms": "Camere da letto",
"general.term.bathrooms": "Bagno Camere",
"general.term.next_step": "Passo successivo",
"general.term.show_password": "Mostra password",
"general.term.hide_password": "Nascondi password",
}

const Links = {
"general.link.add_property": "Aggiungi proprietà",
"general.link.properties": "Le mie proprietà",
}

module.exports = {
    ...Links,
    ...Enum,
    ...Terms
}

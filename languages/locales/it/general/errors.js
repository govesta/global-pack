const Messages = {
"general.error.messages.required": "Questo campo è obbligatorio",
"general.error.messages.common_server": "Si è verificato un errore",
"general.error.messages.already_taken": "Questo già preso",
}

module.exports = {
    ...Messages
}

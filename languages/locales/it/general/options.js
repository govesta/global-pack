const Options = {
"general.options.transaction_type.buy": "Acquistare",
"general.options.transaction_type.rent": "Affitto",
"general.options.transaction_type.lease": "Locazione",
"general.options.transaction_type.sell": "Vendere",

}

module.exports = {
    ...Options
}

const Dashboard = {
    "page.dashboard.hi.title": "Bonjour, {value}",
    "page.dashboard.hi.description": "Ici vous trouvez un récapitulatif des informations importantes sur votre compte Govesta",
    "page.dashboard.listings.title": "Biens",
    "page.dashboard.listings.active": "Biens activés",
    "page.dashboard.listings.pause": "Biens en pause",
    "page.dashboard.listings.total": "Nombre de bien total",
    "page.dashboard.analytics.title": "Analytics",
    "page.dashboard.analytics.link.1": "Ce mois ci",
    "page.dashboard.analytics.link.2": "Les 3 derniers mois",
    "page.dashboard.analytics.link.3": "A vie",
    "page.dashboard.account.title": "Compte",
    "page.dashboard.coming_soon": "(coming soon)",
}

module.exports = {
    ...Dashboard
}

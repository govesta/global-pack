const Login = {
"page.login.title": "Connectez-vous à votre compte",
"page.login.sign_up_text": "Vous n'avez pas de compte?",
"page.login.error": "Email ou mot de passe inccorect",
"page.login.social.facebook.button": "Se connecter avec Facebook",
"page.login.social.google.button": "Se connecter avec Google",
"page.login.forgot_password.link": "Mot de passe oublié?",
}

const Register = {
"page.register.title": "Créer votre compte gratuitement",
"page.register.log_in_text": "Avez-vous un compte?",
"page.register.social.facebook.button": "Se connecter avec Facebook",
"page.register.social.google.button": "Se connecter avec Google",
}

const ProfileEdit = {
"page.profile.edit.general_information.title": "Information générale",
"page.profile.edit.address_information.title": "Adresse",
"page.profile.edit.password_information.title": "Mot de passe",
"page.profile.edit.privacy.title": "Confidentialité",
"page.profile.edit.error.password_match": "Le mot de passe ne correspond pas",
}

const ForgotPassword = {
"page.forgot_password.title": "changez votre mot de passe",
"page.forgot_password.button": "Envoyez le lien de changement",
"page.forgot_password.back_to_login": "Revenir à la page connexion",
"page.forgot_password.success_message": "Le lien vous a été envoyé avec succès",
"page.forgot_password.user_not_found": "Email non trouvé",
}

const ForgotPasswordVerify = {
    "page.forgot_password_verify.title": "Nouveau mot de passe",
    "page.forgot_password_verify.button": "Enregistrer",
    "page.forgot_password_verify.success_message": "Nouveau mot de passe enregistré avec succés ",
    "page.forgot_password_verify.confirm_error": "Ce champ n'est pas le même que le mot de passe",
    }

module.exports = {
    ...Login,
    ...Register,
    ...ProfileEdit,
    ...ForgotPassword,
    ...ForgotPasswordVerify
}

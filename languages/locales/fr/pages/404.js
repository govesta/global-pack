const _404 = {
    "page.404.title": "Oops!",
    "page.404.description": "Nous ne trouvons pas la page que vous cherchez.",
    "page.404.error_code": "Code erreur: {code}",
    "page.404.links_title": "Voici des liens plus utiles:",
    "page.404.link.home": "Home",
    "page.404.link.search": "Recherche"
}

module.exports = {
    ..._404
}

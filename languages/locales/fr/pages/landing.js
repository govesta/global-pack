const AgencyHeader = {
    "page.landing.agency.menu.whygovesta": "Pourquoi Govesta?",
    "page.landing.agency.menu.team": "Notre Equipe",
    "page.landing.agency.menu.help": "Ressources",
    "page.landing.agency.menu.contact": "Contact",
    "page.landing.agency.header.text": "Si vous avez des questions, contactez-nous.",
    "page.landing.agency.header.phone": "Appeler",
    "page.landing.agency.header.email": "E-mail",
}

const Agency = {
    "page.landing.agency.title": "Obtenez du traffic international qualifié sur votre site internet",
    "page.landing.agency.description": "Le moteur de recherche Govesta connecte des acheteurs et des vendeurs dans le monde entier",
    "page.landing.agency.button": "En savoir plus",
        
    "page.landing.agency.challenge.title": "Avancez avec le temps",
    "page.landing.agency.challenge.description": "Nous challengons les modèles et les régles misent en place par les portails immobiliers traditionels.<br><br>✖  Souscription hors de prix<br>✖  Contrat avec engagement<br>✖  Pas d'image de marque<br>✖  Mauvaise expérience utilisateur<br>✖ Disponible en une seule langue",
    
    "page.landing.agency.different.title": "Pourquoi Govesta est différent des autres portails immbiliers",
    "page.landing.agency.different.description": "",
    "page.landing.agency.different.button": "Contactez-nous",
   
    "page.landing.agency.different.1.titel": "Vos clients",
    "page.landing.agency.different.1.description": "Lorsque quelqu'un est intéressé par votre bien immobilier, nous l'envoyons directement sur votre site web. Après tout, ce sont vos clients, pas les nôtres !",
    "page.landing.agency.different.2.titel": "International",
    "page.landing.agency.different.2.description": "Acheter n'importe où, vendre n'importe où. Notre site web est disponible en plusieurs langues et nous en ajoutons chaque mois.",
    "page.landing.agency.different.3.titel": "Juste & Flexible",
    "page.landing.agency.different.3.description": "C'est vous qui décidez quand vous voulez faire de la publicité. Il n'y a pas d'engagement à long terme, pas de frais d'abonnement, et çà restera comme cela",

    "page.landing.agency.grow.title": "Comprendre vos clients",
    "page.landing.agency.grow.description": "Govesta amène les gens sur votre site, afin que vous puissiez leur montrer ce qui vous êtes.",
    "page.landing.agency.grow.quote": "“Govesta renvoi les acheteurs potentiels sur mon propre site internet, là ou je les veux! <br>Je peux montrer mon contenu, recibler mes visiteurs et économiser de l'argent sur le SEM, car Govesta s'occupe de cela pour moi.”",
    "page.landing.agency.grow.quotedesc": "Thirzie Hull –  Real Estate Agent, London",
    
    "page.landing.agency.setup.title": "Pour démarrer, c'est très facile",
    "page.landing.agency.setup.description": "Nous sommes synchronisés avec Openimmo, somme connecté sur des plateformes comme Apimo, AC3 et Propstack. Nous vous aidons à trouver la meilleure configuration.",
    
    "page.landing.agency.trial.title": "Contactez nous",
    "page.landing.agency.trial.description": "Demandez-nous n'importe quoi ! Nous vous répondrons dans les plus brefs délais.",

    "page.landing.agency.team.title": "Notre équipe",
    "page.landing.agency.team.description": "⠀",

    "page.landing.agency.faq.title": "Questions fréquentes",

    "page.landing.agency.faq.question1": "Combien çà coute de publier vos listings sur Govesta?",
    "page.landing.agency.faq.answer1": "Le téléchargement de vos bien immobiliers sur Govesta est entièrement gratuit et illimité. Vous ne payez que lorsqu'une personne clique sur l'une de vos annonces et se rend sur votre site.",

    "page.landing.agency.faq.question2": "Puis-je publier sur d'autres plateformes?",
    "page.landing.agency.faq.answer2": "Oui vous pouvez publier sur autant de plateforme que vous le souhaitez.",

    "page.landing.agency.faq.question3": "Y-a t-il une limite au nombre de bien que je peux publier?",
    "page.landing.agency.faq.answer3": "Les téléchargements sont illimités, vous pouvez télécharger autant de biens immobilier que vous le souhaitez et mettre en pause ou arrêter l'annonce à tout moment.",

    "page.landing.agency.faq.question4": "Puis-je mettre un budget maximum par bien immobilier?",
    "page.landing.agency.faq.answer4": "Vous pouvez à tout moment mettre en pause ou arrêter chaque listing individuellement, mais vous ne pouvez pas fixer de budget maximum.",

    "page.landing.agency.faq.question5": "Est ce que j'ai besoin de mon site internet pour publier sur Govesta?",
    "page.landing.agency.faq.answer5": "Govesta a pour objectifs de renvoyer les acheteurs directement sur votre site internet, vous en avez donc bien besoin.",
    "page.landing.agency.contact.submit": "Contactez nous",
    "page.landing.agency.contact.success_message": "Envoyé!",
    
    "page.landing.agency.contact.title": "Contactez-nous",
    "page.landing.agency.contact.decription": "  ",



    "page.landing.agency.help.title": "Laissez-nous vous aider à tirer le meilleur parti de Govesta",
    "page.landing.agency.help.button": "En savoir plus",


    "page.landing.agency.why.1.title": "1. Publiez tous vos bien immobiliers, gratuitement",
    "page.landing.agency.why.1.text": "Que vous ayez 1 bien à vendre ou 1000, vous pouvez télécharger autant d'annonces que vous le souhaitez, gratuitement. Nous ne facturons aucun frais de démarrage ou d'abonnement. Nous sommes convaincus que ce qui compte, c'est la performance de chaque annonce.",
    "page.landing.agency.why.2.title": "2. C'est vous qui choisissez",
    "page.landing.agency.why.2.text": "Nous ne vous obligeons pas à vous engager dans un contrat de publicité pour un ou deux ans. C'est pourquoi votre partenariat avec nous peut être interrompu à tout moment et pour n'importe quelle raison. Vous pouvez gérer toutes vos annonces individuellement dans le tableau de bord de Govesta, afin de toujours garder le contrôle de vos coûts.",
    "page.landing.agency.why.3.title": "3. Objetez vos clients directement sur votre site internet",
    "page.landing.agency.why.3.text": "Combien avez-vous investi dans votre tout nouveau site web ? Si vous êtes comme nous, c'est probablement beaucoup ! Vos clients devraient avoir la chance de le voir, n'est-ce pas ? Lorsque quelqu'un montre un intérêt pour vos biens immobiliers, Govesta l'envoie directement sur votre site web. Cela crée de nombreuses nouvelles possibilités. Par exemple, vous pouvez optimiser votre site web en fonction du comportement des utilisateurs. Vous pouvez recibler vos visiteurs, ou créer des publics similaires sur d'autres canaux publicitaires",
    "page.landing.agency.why.4.title": "4. Acheteurs internationaux",
    "page.landing.agency.why.4.text": "Achetez partout, vendez partout ! C'est notre vision. Notre clientèle est internationale et provient principalement des pays européens. Avec Govesta, vous serez présent sur les meilleurs marchés immobiliers du monde, ce qui vous donnera un accès facile aux acheteurs étrangers. Notre équipe a plus de 10 ans d'expérience professionnelle dans le domaine de la publicité numérique. Nous savons comment trouver les bonnes personnes pour vos biens immobiliers.",
    "page.landing.agency.why.5.title": "5. Nous sommes connectés au meilleur CRM Européen",
    "page.landing.agency.why.5.text": "Nous sommes connectés avec les meilleurs fournisseurs de logiciels de gestion immobilière, notamment OnOffice, Flowfact, Apimo, AC3 et Propstack. Et si vous avez plus de 100 biens à publier, nous développerons une API entièrement connectée avec votre base de données sans frais supplémentaires.",
    "page.landing.agency.why.6.title": "6. Suivez vos performances en temps réel",
    "page.landing.agency.why.6.text": "Nous sommes une entreprise de marketing en ligne. Nous savons qu'une approche fondée sur les données est la clé du succès d'une campagne publicitaire. C'est pourquoi nous donnons à chaque annonceur un accès gratuit à notre tableau de bord des performances, où vous pouvez suivre en temps réel les performances des biens immobiliers. Comprenez d'où viennent vos visiteurs, mais aussi d'où proviennent vos coûts, et optimiser.",
    "page.landing.agency.why.7.title": "7. Vos biens immobiliers, sur le meilleur produit",
    "page.landing.agency.why.7.text": "La mission de Govesta est d'être la meilleure plateforme de recherche de biens immobiliers au monde. Pour y parvenir, nous nous concentrons sur la création de la meilleure expérience utilisateur possible, afin que les gens puissent trouver la maison de leurs rêves en quelques clics seulement. Il est tout aussi important de créer une grande marque. Nous développons notre propre marque grâce à un contenu unique et de haute qualité, que nous publions sur le journal Govesta. La publicité sur Govesta amplifie votre marque, plutôt que de l'affaiblir.",
    "page.landing.agency.why.8.title": "8. Des experts à votre disposition",
    "page.landing.agency.why.8.text": "Une fois que vous travaillez avec Govesta, notre équipe d'experts est heureuse d'analyser votre site web et de vous donner des conseils sur la façon de l'améliorer. Parfois, un petit changement peut faire une grande différence. Profitez de conseils d'experts gratuits en travaillant avec Govesta.",
    "page.landing.agency.why.9.title": "9. Rejoignez le futur de l'immobilier avec Govesta",
    "page.landing.agency.why.9.text": "Nous sommes jeunes, frais et prêts à révolutionner le marché de l'immobilier. Après 10 ans passés dans le secteur en ligne, l'équipe de Govesta se réunit pour construire une puissante plateforme pour les acheteurs et les vendeurs. Au cours des deux prochaines années, nous prévoyons une très forte croissance européenne et nous voulons que nos premiers partenaires restent les plus proches.",
    "page.landing.agency.why.10.title": "10. Testez Govesta gratuitement",
    "page.landing.agency.why.10.text": "Inscrivez-vous à Govesta et obtenez vos 100 premiers clients gratuitement. Il n'y a pas d'engagement à long terme, et donc aucun risque. Nous sommes impatients de vous avoir à bord!",

}

module.exports = {
    ...Agency,
    ...AgencyHeader
}

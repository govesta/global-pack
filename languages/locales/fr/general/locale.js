const Languages = {
    "general.language.en": "Anglais",
    "general.language.de": "Allemand",
    "general.language.es": "Espagnol",
    "general.language.fr": "Français",
    "general.language.it": "Italien"
}

module.exports = {
    ...Languages
}
const Messages = {
"general.error.messages.required": "Ce champ est obligatoire",
"general.error.messages.common_server": "Une erreure s'est produite",
"general.error.messages.already_taken": "Déjà pris",
"general.error.messages.confirm": "Ce champ n'est pas le même {value}",
}

module.exports = {
    ...Messages
}


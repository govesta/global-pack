﻿const Cookie = {
    "component.eucookie.text": "Govesta utilise des cookies pour vous fournir la meilleur expérience utilisateur. Pour que Govesta fonctionne, nous partageons les données des utilisateurs avec nos processeurs. Afin d'utiliser Govesta, vous devez accepter nos <a>Privacy Policy</a>.",
    "component.eucookie.button": "J'accepte."
}
const Address = {
    "component.address.placeholder": "Cherchez un ville ou un quartier",
    "component.address.placeholder_question": "Où souhaitez-vous acheter?"
}
const PropertyCard = {
    "component.property_card.view": "En savoir plus"
}
 
module.exports = {
    ...Cookie,
    ...Address,
    ...PropertyCard
}


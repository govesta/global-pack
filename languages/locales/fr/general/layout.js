const Header = {
    "layout.header.links.agency": "Publier",
    "layout.header.links.magazine": "Journal",
    "layout.header.links.dashboard": "Se connecter ou nous rejoindre",
    "layout.header.menu.apartments": "Appartements",
    "layout.header.menu.houses": "Maisons",
    "layout.header.menu.holyday_homes": "Maison de vacances",
    "layout.header.menu.luxury_properties": "Biens de luxe",
}

const Footer = {
    "layout.footer.copyright": "© Govesta SAS",
    "layout.footer.links.imprint": "Mentions légales",
    "layout.footer.links.terms": "Termes",
    "layout.footer.links.privacy_policy": "Politique de confidentialité",
}

const Seo = {
    "layout.seo.general.title": "Home",
    "layout.seo.general.description": "Trouvez votre futur chez vous ,partout dans le monde avec Govesta. ✓ Maisons, ✓ Appartements ✓ Biens commerciaux.",
    "layout.seo.search.title": "Chercher",
    "layout.seo.search.title.with_location": "{location}",
    "layout.seo.page.title": "{page}",
    "layout.seo.page.search.title": "Biens immobiliers à vendre à {location} - {number_of_properties} biens à vendre",
    "layout.seo.page.search.meta": "Trouvez des biens immobiliers à vendre à {location} avec Govesta.co. Choisissez parmis {number_of_properties} biens immobiliers. Utilisez nos filtres pour trouver votre nouveau chez vous.",
}

const Forward = {
    "layout.forward.text": "Vous avez trouvé un bien qui vous plaît sur Govesta.<br/><br/>Nous vous redirigeons vers {value}"
}


module.exports = {
    ...Seo,
    ...Header,
    ...Footer,
    ...Forward
}

const Options = {
    "general.options.transaction_type.buy": "Acheter",
    "general.options.transaction_type.rent": "Louer",
    "general.options.transaction_type.lease": "Louer",
    "general.options.transaction_type.sell": "Vendre",
}

const PropertyCategory = {
    "general.options.property.features.category.luxury": "Luxe",    
    "general.options.property.features.category.simple": "Simple",    
    "general.options.property.features.category.upscale": "Haut de gamme",    
    "general.options.property.features.category.normal": "Standard"
}

const PropertyParking = {
    "general.options.property.features.parking.carport": "Carport",    
    "general.options.property.features.parking.duplex": "Duplex",    
    "general.options.property.features.parking.openspace": "Open space",    
    "general.options.property.features.parking.garage": "Garage",    
    "general.options.property.features.parking.parkinggarage": "Parking garage",    
    "general.options.property.features.parking.undergroundparking": "Parking en sous sol",    
    "general.options.property.features.parking.other": "Autres"
}

const PropertyState = {
    "general.options.property.features.state.brandnew": "Neuf",    
    "general.options.property.features.state.demolitionobject": "A démolir",    
    "general.options.property.features.state.buildingdue": "Livraison prévue",    
    "general.options.property.features.state.decored": "Décoré",    
    "general.options.property.features.state.projected": "En projet",    
    "general.options.property.features.state.neu": "Neuf",    
    "general.options.property.features.state.fullyrenovate": "Rénové"
}

const PropertyFeatures = {
    "general.options.property.features.balcony": "Balcon",
    "general.options.property.features.bathtub": "Baignoire",
    "general.options.property.features.conceirge": "Concierge",
    "general.options.property.features.design_home": "Design",
    "general.options.property.features.disabled_access": "Accès handicapé",
    "general.options.property.features.elevator": "Ascenseur",
    "general.options.property.features.farm": "Ferme",
    "general.options.property.features.fireplace": "Cheminée",
    "general.options.property.features.fitted_kitchen": "Cuisine équipée",
    "general.options.property.features.furnished": "Meublé",
    "general.options.property.features.garage": "Garage",
    "general.options.property.features.garden": "Jardin",
    "general.options.property.features.holiday_home": "Maison de vacance",
    "general.options.property.features.land": "Terrain",
    "general.options.property.features.new_home": "Neuf",
    "general.options.property.features.parking_space": "Parking",
    "general.options.property.features.period_home": "Maison secondaire",
    "general.options.property.features.pool": "Piscine",
    "general.options.property.features.retirement_home": "Maison de retraite",
    "general.options.property.features.roof_terrace": "Terasse sur le toît",
    "general.options.property.features.serviced": "Servis ",
    "general.options.property.features.suitable_for_families": "Convient aux familles",
    "general.options.property.features.suitable_for_pets": "Convient pour un chien",
    "general.options.property.features.suitable_for_sharers": "Convient pour la colocation",
    "general.options.property.features.suitable_for_students": "Convient pour les étudiants",
    "general.options.property.features.terrace ": "Terasse ",
    "general.options.property.features.unfurnished": "Non Meublé",  

    "general.options.property.features.category.luxury": "Luxe",    
    "general.options.property.features.category.simple": "Simple",    
    "general.options.property.features.category.upscale": "Haut de gamme",    
    "general.options.property.features.category.normal": "Standard",   
    
    "general.options.property.features.parking.carport": "Carport",    
    "general.options.property.features.parking.duplex": "Duplex",    
    "general.options.property.features.parking.openspace": "Open space",    
    "general.options.property.features.parking.garage": "Garage",    
    "general.options.property.features.parking.parkinggarage": "Parking garage",    
    "general.options.property.features.parking.undergroundparking": "Parking sous sol",    
    "general.options.property.features.parking.other": "Autre",    

    "general.options.property.features.state.new": "Neuf",    
    "general.options.property.features.state.demolitionobject": "Projet de démolition",    
    "general.options.property.features.state.buildingdue": "Livraison prévue",    
    "general.options.property.features.state.decored": "Décoré",    
    "general.options.property.features.state.projected": "En projet",    
    "general.options.property.features.state.new": "Neuf",    
    "general.options.property.features.state.fullyrenovate": "Rénové",    
    

}

module.exports = {
    ...Options,
    ...PropertyFeatures,
    ...PropertyCategory,
    ...PropertyParking,
    ...PropertyState
}

const pages = require('./pages');
const general = require('./general');

module.exports = {
    ...general,
    ...pages
}
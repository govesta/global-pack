const Header = {
    "layout.header.links.agency": "Advertise",
    "layout.header.links.magazine": "Journal",
    "layout.header.links.dashboard": "Sign in or Join",
    "layout.header.menu.apartments": "Apartments",
    "layout.header.menu.houses": "Houses",
    "layout.header.menu.holyday_homes": "Holiday homes",
    "layout.header.menu.luxury_properties": "Luxury properties",
}

const Footer = {
    "layout.footer.copyright": "© getmynewhome UG",
    "layout.footer.links.imprint": "Imprint",
    "layout.footer.links.terms": "Terms",
    "layout.footer.links.privacy_policy": "Privacy Policy",
}

const Seo = {
    "layout.seo.general.title": "Home",
    "layout.seo.general.description": "Find your perfect new home anywhere in the world with Govesta. ✓ Houses, ✓ apartments ✓ commercial properties to buy and rent.",
    "layout.seo.search.title": "Search",
    "layout.seo.search.title.with_location": "{location}",
    "layout.seo.page.title": "{page}",
    "layout.seo.page.search.title": "Properties for sale in {location} - {number_of_properties} properties for sale",
    "layout.seo.page.search.meta": "Find properties for sale in {location} with Govesta.co. Choose from our {number_of_properties} properties. Use our filters to find your new home.",
}

const Forward = {
    "layout.forward.text": "You have found a great property on Govesta.<br/><br/>We are now redirecting you to {value}"
}


module.exports = {
    ...Seo,
    ...Header,
    ...Footer,
    ...Forward
}

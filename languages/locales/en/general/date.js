const Date = {
    "general.date.day": "{value, plural, one {# day} other {# days}}"
}
 
module.exports = {
    ...Date
}

const Options = {
    "general.options.transaction_type.buy": "Buy",
    "general.options.transaction_type.rent": "Rent",
    "general.options.transaction_type.lease": "Lease",
    "general.options.transaction_type.sell": "Sell",
}

const PropertyCategory = {
    "general.options.property.features.category.luxury": "Luxury",    
    "general.options.property.features.category.simple": "Simple",    
    "general.options.property.features.category.upscale": "Upscale",    
    "general.options.property.features.category.normal": "Normal"
}

const PropertyParking = {
    "general.options.property.features.parking.carport": "Carport",    
    "general.options.property.features.parking.duplex": "Duplex",    
    "general.options.property.features.parking.openspace": "Open space",    
    "general.options.property.features.parking.garage": "Garage",    
    "general.options.property.features.parking.parkinggarage": "Parking garage",    
    "general.options.property.features.parking.undergroundparking": "Underground parking",    
    "general.options.property.features.parking.other": "Other"
}

const PropertyState = {
    "general.options.property.features.state.brandnew": "Brand new",    
    "general.options.property.features.state.demolitionobject": "Demolition object",    
    "general.options.property.features.state.buildingdue": "Building due",    
    "general.options.property.features.state.decored": "Decored",    
    "general.options.property.features.state.projected": "Projected",    
    "general.options.property.features.state.neu": "New",    
    "general.options.property.features.state.fullyrenovate": "Fully Renovate"
}

const PropertyFeatures = {
    "general.options.property.features.balcony": "Balcony",
    "general.options.property.features.bathtub": "Bathtub",
    "general.options.property.features.conceirge": "Conceirge",
    "general.options.property.features.design_home": "Design home",
    "general.options.property.features.disabled_access": "Disabled access",
    "general.options.property.features.elevator": "Elevator",
    "general.options.property.features.farm": "Farms",
    "general.options.property.features.fireplace": "Fireplace",
    "general.options.property.features.fitted_kitchen": "Fitted kitchen",
    "general.options.property.features.furnished": "Furnished",
    "general.options.property.features.garage": "Garage",
    "general.options.property.features.garden": "Garden",
    "general.options.property.features.holiday_home": "Holiday home",
    "general.options.property.features.land": "Land",
    "general.options.property.features.new_home": "New home",
    "general.options.property.features.parking_space": "Parking space",
    "general.options.property.features.period_home": "Period home",
    "general.options.property.features.pool": "Pool",
    "general.options.property.features.retirement_home": "Retirement home",
    "general.options.property.features.roof_terrace": "Roof terrace",
    "general.options.property.features.serviced": "Serviced ",
    "general.options.property.features.suitable_for_families": "suitable for Families",
    "general.options.property.features.suitable_for_pets": "suitable for Pets",
    "general.options.property.features.suitable_for_sharers": "suitable for Sharers",
    "general.options.property.features.suitable_for_students": "suitable for Students",
    "general.options.property.features.terrace ": "Terrace ",
    "general.options.property.features.unfurnished": "Unfurnished",  

    "general.options.property.features.category.luxury": "Luxury",    
    "general.options.property.features.category.simple": "Simple",    
    "general.options.property.features.category.upscale": "Upscale",    
    "general.options.property.features.category.normal": "Normal",   
    
    "general.options.property.features.parking.carport": "Carport",    
    "general.options.property.features.parking.duplex": "Duplex",    
    "general.options.property.features.parking.openspace": "Open space",    
    "general.options.property.features.parking.garage": "Garage",    
    "general.options.property.features.parking.parkinggarage": "Parking garage",    
    "general.options.property.features.parking.undergroundparking": "Underground parking",    
    "general.options.property.features.parking.other": "Other",    

    "general.options.property.features.state.new": "Brand new",    
    "general.options.property.features.state.demolitionobject": "Demolition object",    
    "general.options.property.features.state.buildingdue": "Building due",    
    "general.options.property.features.state.decored": "Decored",    
    "general.options.property.features.state.projected": "Projected",    
    "general.options.property.features.state.new": "New",    
    "general.options.property.features.state.fullyrenovate": "Fully Renovate",    
    

}

module.exports = {
    ...Options,
    ...PropertyFeatures,
    ...PropertyCategory,
    ...PropertyParking,
    ...PropertyState
}

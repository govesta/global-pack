const Messages = {
"general.error.messages.required": "This field is required",
"general.error.messages.common_server": "An error has occurred",
"general.error.messages.already_taken": "This is already taken",
"general.error.messages.confirm": "This field is not the same with {value}",
}

module.exports = {
    ...Messages
}


﻿const Cookie = {
    "component.eucookie.text": "Govesta uses browser cookies to give you the best possible experience. For Govesta to work, we log user data and share it with processors. To use Govesta, you must agree to our <a>Privacy Policy</a>.",
    "component.eucookie.button": "I agree."
}
const Address = {
    "component.address.placeholder": "Search for Town, City, or District",
    "component.address.placeholder_question": "Where do you want to buy?"
}
const PropertyCard = {
    "component.property_card.view": "View Details"
}
 
module.exports = {
    ...Cookie,
    ...Address,
    ...PropertyCard
}


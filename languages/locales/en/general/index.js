const options = require('./options');
const errors = require('./errors');
const general = require('./general');
const locale = require('./locale');
const component = require('./components');
const layout = require('./layout');
const date = require('./date');
const subTypePlural = require('./subTypePlural');

module.exports = {
    ...layout,
    ...component,
    ...locale,
    ...general,
    ...options,
    ...errors,
    ...date,
    ...subTypePlural
}

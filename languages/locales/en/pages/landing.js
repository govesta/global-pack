const AgencyHeader = {
    "page.landing.agency.menu.whygovesta": "Why Govesta?",
    "page.landing.agency.menu.team": "Our Team",
    "page.landing.agency.menu.help": "Resources",
    "page.landing.agency.menu.contact": "Contact",
    "page.landing.agency.header.text": "If you have any questions, contact us.",
    "page.landing.agency.header.phone": "Phone Call",
    "page.landing.agency.header.email": "E-mail",
}

const Agency = {
    "page.landing.agency.title": "Get qualified, international buyers on your website",
    "page.landing.agency.description": "Govesta property search connects buyers and sellers all over the world",
    "page.landing.agency.button": "Learn More",
        
    "page.landing.agency.challenge.title": "Get with the times",
    "page.landing.agency.challenge.description": "We are challenging the rules made by traditional property portals.<br><br>✖  Overpriced subscriptions<br>✖  Long-term contracts<br>✖  No brand engagement<br>✖  Bad user experience<br>✖  No foreign languages",
    
    "page.landing.agency.different.title": "What makes Govesta different to other property portals?",
    "page.landing.agency.different.description": "",
    "page.landing.agency.different.button": "Get in Touch",
   
    "page.landing.agency.different.1.titel": "Your Customer",
    "page.landing.agency.different.1.description": "When somebody is interested in your property, we send them directly to your website. After all, they are your customer, not ours!",
    "page.landing.agency.different.2.titel": "International",
    "page.landing.agency.different.2.description": "Buy anywhere, sell anywhere. Our website is available in multiple languages and we are adding more every month.",
    "page.landing.agency.different.3.titel": "Fair & Flexible",
    "page.landing.agency.different.3.description": "You decide when you want to advertise. There is no long-term commitment, no subscription fees, and that's the way it will always be.",

    "page.landing.agency.grow.title": "Understand your customers",
    "page.landing.agency.grow.description": "Govesta brings people to your site, so you can show them what you are all about.",
    "page.landing.agency.grow.quote": "“Govesta brings potential buyers to my website, which is where I want them! <br>I can show off my content, retarget my visitors and save money on SEM, because Govesta takes care of that for me.”",
    "page.landing.agency.grow.quotedesc": "Thirzie Hull –  Real Estate Agent, London",
    
    "page.landing.agency.setup.title": "Getting started is easy",
    "page.landing.agency.setup.description": "We are synchronized with Openimmo, used on platforms like OnOffice, FlowFact and Propstack. We'll help you find the best set-up.",
    
    "page.landing.agency.trial.title": "Get in touch",
    "page.landing.agency.trial.description": "Ask us anything! We'll get back to you right away.",

    "page.landing.agency.team.title": "Meet Our Team",
    "page.landing.agency.team.description": "⠀",

    "page.landing.agency.faq.title": "Frequently asked questions",

    "page.landing.agency.faq.question1": "How much does it cost to advertise with Govesta?",
    "page.landing.agency.faq.answer1": "Uploading your properties to Govesta is completely free. You only pay when a someone clicks on one of your listings and comes to your website.",

    "page.landing.agency.faq.question2": "Can I advertise on other platforms?",
    "page.landing.agency.faq.answer2": "Yes you can advertise on as many other platforms as you like.",

    "page.landing.agency.faq.question3": "Is there a limit to the number of properties I can upload at any one time?",
    "page.landing.agency.faq.answer3": "Uploads are limitless, you can upload as many properties as you like and pause or stop the advert at any time.",

    "page.landing.agency.faq.question4": "Can I set a maximum budget for my listings?",
    "page.landing.agency.faq.answer4": "You can pause or stop each individual listing at any time, but you cannot set a maximum budget.",

    "page.landing.agency.faq.question5": "Do I need my own website to advertise with Govesta?",
    "page.landing.agency.faq.answer5": "Govesta is all about connecting home hunters with agents, you will need a website in order to showcase your portfolio.",
    "page.landing.agency.contact.submit": "Get in contact",
    "page.landing.agency.contact.success_message": "sent!",
    
    "page.landing.agency.contact.title": "Get in Touch",
    "page.landing.agency.contact.decription": "  ",



    "page.landing.agency.help.title": "Lets us Help you to get the most out of Govesta",
    "page.landing.agency.help.button": "More",


    "page.landing.agency.why.1.title": "1. Upload All Of Your Listings, For Free",
    "page.landing.agency.why.1.text": "Whether you’ve got 1 property for sale or 1000, you can upload as many listings as you want, for free. We do not charge any start-up or subscription fees. We are convinced that the performance of each listing is what matters.",
    "page.landing.agency.why.2.title": "2. Advertise On Your Terms",
    "page.landing.agency.why.2.text": "We don’t make you commit to an advertising contract for one or two years. That’s why your partnership with us can be paused at any time and for any reason. You can manage all your listings individually in the Govesta Dashboard, so you always remain in control of your costs.",
    "page.landing.agency.why.3.title": "3. Get the client directly on your website",
    "page.landing.agency.why.3.text": "How much have you invested in your brand new website? If you are like us, then it is probably a lot! Your clients should have the chance to see it, right? When somebody shows an interest in your property, Govesta sends them directly to your website. This creates many new possibilities. For example, you can optimise your website according to user behaviour. You can retarget your visitors, or create lookalike audiences on other advertising channels.",
    "page.landing.agency.why.4.title": "4. International Buyers",
    "page.landing.agency.why.4.text": "Buy anywhere, sell anywhere! That’s our vision. Our customer base is international, coming mostly from European countries. With Govesta, you’ll have a presence in the best property markets around the world, giving you easy access to foreign buyers. Our team has over 10 years of work experience in digital advertising. We know how to find the right people for your property.",
    "page.landing.agency.why.5.title": "5. Connected With The Best Property Management Software",
    "page.landing.agency.why.5.text": "We are connected with the best property management software providers, including OnOffice, Flowfact, and Propstack. And if you have more than 100 objects to advertise, we will develop a fully connected API with your database at no additional cost.",
    "page.landing.agency.why.6.title": "6. Track Your Performance In Real-Time",
    "page.landing.agency.why.6.text": "We are an online marketing tech company. We understand that a data-driven approach is what makes a successful advertising campaign. That’s why we give every advertiser free access to our performance dashboard, where you can track property performance in real-time. Understand where your visitors are coming from, but also where your costs are coming from, and optimize.",
    "page.landing.agency.why.7.title": "7. Your Listings On The Best Product",
    "page.landing.agency.why.7.text": "Govesta’s mission is to be the best property search platform in the world. To achieve this, we focus on creating the best user experience possible, so people can find their dream home in just a few clicks. Equally important is creating a great brand. We develop our own brand through high-quality, unique content, which we publish on the Govesta Journal. Advertising on Govesta amplifies your brand, rather than diminishing it.",
    "page.landing.agency.why.8.title": "8. Expert Advice To Improve Your Website",
    "page.landing.agency.why.8.text": "Once you work with Govesta, our team of experts is happy to analyze your website and give advice on how to improve. Sometimes just a small change can make a huge difference. Enjoy free, expert consulting while working with Govesta.",
    "page.landing.agency.why.9.title": "9. Join The Next Big Thing In Real Estate",
    "page.landing.agency.why.9.text": "We are young, fresh and ready to revolutionize the real estate market. After 10 years spent in the online industry, the Govesta team is putting their heads together to build a powerful platform for buyers and sellers. Over the next 2 years, we are planning very strong European growth and we want our first partners to remain the closest.",
    "page.landing.agency.why.10.title": "10. Test Govesta for free",
    "page.landing.agency.why.10.text": "Sign-up to Govesta and get your first 100 customers for free. There is no long-term commitment, and so there is no risk. We can’t wait to have you on board!",

}

module.exports = {
    ...Agency,
    ...AgencyHeader
}

const property = require('./property');
const login = require('./login');
const home = require('./home');
const _404 = require('./404');
const landing = require('./landing');
const dashboard = require('./dashboard');

module.exports = {
    ...home,
    ...property,
    ...login,
    ..._404,
    ...landing,
    ...dashboard
}
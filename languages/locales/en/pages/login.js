const Login = {
"page.login.title": "Log-in to your Account",
"page.login.sign_up_text": "Don’t have an account?",
"page.login.error": "Email or Password is not correct",
"page.login.social.facebook.button": "Log in with Facebook",
"page.login.social.google.button": "Log in with Google",
"page.login.forgot_password.link": "Forgot Password?",
}

const Register = {
"page.register.title": "Get started with a free account",
"page.register.log_in_text": "Do you have an account?",
"page.register.social.facebook.button": "Sign up with Facebook",
"page.register.social.google.button": "Sign up with Google",
}

const ProfileEdit = {
"page.profile.edit.general_information.title": "General Information",
"page.profile.edit.address_information.title": "Address Information",
"page.profile.edit.password_information.title": "Password Information",
"page.profile.edit.privacy.title": "Privacy",
"page.profile.edit.error.password_match": "Passwords don't match",
}

const ForgotPassword = {
"page.forgot_password.title": "Reset password",
"page.forgot_password.button": "Send reset link",
"page.forgot_password.back_to_login": "Back to Login",
"page.forgot_password.success_message": "Successfully sent reset link",
"page.forgot_password.user_not_found": "Email Not Found",
}

const ForgotPasswordVerify = {
    "page.forgot_password_verify.title": "New Password",
    "page.forgot_password_verify.button": "Save",
    "page.forgot_password_verify.success_message": "Successfully set your new password ",
    "page.forgot_password_verify.confirm_error": "This field is not same with password",
    }

module.exports = {
    ...Login,
    ...Register,
    ...ProfileEdit,
    ...ForgotPassword,
    ...ForgotPasswordVerify
}

const _404 = {
    "page.404.title": "Oops!",
    "page.404.description": "We can't seem to find the page you're looking for.",
    "page.404.error_code": "Error code: {code}",
    "page.404.links_title": "Here are some helpful links instead:",
    "page.404.link.home": "Home",
    "page.404.link.search": "Search"
}

module.exports = {
    ..._404
}

const Dashboard = {
    "page.dashboard.hi.title": "Hi, {value}",
    "page.dashboard.hi.description": "Here you find an overview over all important things to do with your Govesta account",
    "page.dashboard.listings.title": "Listings",
    "page.dashboard.listings.active": "Active Listings",
    "page.dashboard.listings.pause": "Pause Listings",
    "page.dashboard.listings.total": "Total Listings",
    "page.dashboard.analytics.title": "Analytics",
    "page.dashboard.analytics.link.1": "Current Month",
    "page.dashboard.analytics.link.2": "Last 3 Month",
    "page.dashboard.analytics.link.3": "Lifetime",
    "page.dashboard.account.title": "Account",
    "page.dashboard.coming_soon": "(coming soon)",
}

module.exports = {
    ...Dashboard
}
